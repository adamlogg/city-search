import React from 'react'
import './App.css'

import SearchPage from './pages/searchPage'


function App() {
  return (
    <React.StrictMode>
      <div className="App">
        <SearchPage />
      </div>
    </React.StrictMode>
  )
}

export default App
