import React, {useState} from 'react'


export function SearchBar({
    startSearch, disabled=false, warningReporter=null, placeholder=''}) {
    /*  This component requires 'startSearch' callback function to be fired when
        'search' button is clicked.
        Optional function 'warningReporter' is fired in case of invalid input.  */
    
    const [searchText, setSearchText] = useState('')

    function changeSearchText(event) {
        const {value} = event.target
        const isValid = value.match(/^[a-z]*$/i)
        if (isValid) {
            setSearchText(value)
        } else {
            showWarning('Please provide the valid input. Only letters a-z, A-Z can be entered.')
        }
    }

    function showWarning(warningText) {
        if (warningReporter) {
            warningReporter(warningText)
        }
    }

    function startSearchCallback(event) {
        event.preventDefault()
        // check if text not empty
        if (searchText) {
            startSearch(searchText)
        } else {
            showWarning('Search text cannot be empty.')
        }
    }

    return (
        <form className='search-container' onSubmit={startSearchCallback}>
            <input
                type='text'
                placeholder={placeholder}
                className='search-field'
                value={searchText}
                onChange={changeSearchText}
            />
            <button
                type='submit'
                className='search-button'
                disabled={disabled}
            > Search </button>
        </form>
    )
}