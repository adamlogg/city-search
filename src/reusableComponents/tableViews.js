import React from 'react'


export function TableView({data, color='black'}) {
    /*  This component requires 'data' object, conaining arrays of cells. Keys will be
        treated as headers. 
    */

    const gridItemStyle = {
        margin: '0.15rem',
        border: `1px solid ${color}`,
        padding: '0.5rem 0.75rem',
    }
    const headerStyle = {
        ...gridItemStyle,
        background: color,
        fontWeight: 'bolder',
        color: 'white',
        border: 'none',
    }

    let maxRowLen = 0
    let rowNum = 0
    let key = 0
    const cellComponents = []
    Object.entries(data).forEach(([header, cells]) => {
        if (cells.length > maxRowLen) {
            maxRowLen = cells.length
        }
        const gridRow = `${++rowNum} / span 1`
        cellComponents.push(
            <div key={key++} style={{...headerStyle, gridRow}}>{header}</div>
        )
        cellComponents.push(cells.map(city => (
            <div key={key++} style={{...gridItemStyle, gridRow}}>{city}</div>
        )))
    })

    const gridStyle = {
        display: 'grid',
        gridTemplateColumns: `repeat(${maxRowLen + 1}, 1fr)`,
        margin: '0.75em',
    }

    return (
        <div style={gridStyle}>
            {cellComponents}
        </div>
    )
}