import React from 'react'
import {useSelector, useDispatch} from 'react-redux'

import {SearchBar} from '../reusableComponents/searchBar'
import {Loader} from '../reusableComponents/loaders'
import {TableView} from '../reusableComponents/tableViews'
import {fetchCities} from '../redux/actions/citiesActions'
import {showWarning} from '../redux/actions/uiActions'


const PRIMARY_COLOR = '#0b5a96'
// const SECONDARY_COLOR = '#032b49'

export default function SearchPage() {
    /*  A component specific to this project, wrapping reusable search components,
        responsible for interfacing redux store, passing props and dispatch actions.  */

    const dispatch = useDispatch()
    const {cities, fetchingInProgress} = useSelector(state => state.cities)
    
    const citiesFound = cities.length
    const citiesByState = getCitiesByState(cities)

    function startSearch(searchText) {
        dispatch(fetchCities(searchText))
    }

    return (
        <>
            <header className="App-header">
                <SearchBar
                    startSearch={startSearch}
                    disabled={fetchingInProgress}
                    warningReporter={(text) => dispatch(showWarning(text))}
                    placeholder='Search for a city...'
                />
            </header>
            <div className='search-results'>
                {fetchingInProgress
                    ? <Loader />
                    : <>
                        <h1>Total cities found: {citiesFound}</h1>
                        <TableView data={citiesByState} color={PRIMARY_COLOR}/>
                    </>
                }
            </div>
        </>
    )
}

function getCitiesByState(cities) {
    /*  Accepts an array of [city, state] arrays.
        Returns an object with 'states' as keys and arrays
        of cities belonging to each state as values.  */

    const citiesByState = {}
    for (const {city, state} of cities) {
        if (citiesByState[state]) {
            citiesByState[state].push(city)
        } else {
            citiesByState[state] = [city]
        }
    }
    return citiesByState
}