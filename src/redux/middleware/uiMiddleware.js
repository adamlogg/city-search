import {
    SHOW_ERROR,
    SHOW_WARNING,
} from '../actions/uiActions'


export const showErrorProcess = ({dispatch}) => next => action => {
    next(action)
  
    if (action.type === SHOW_ERROR) {
        alert(action.payload)
    }
}

export const showWarningProcess = ({dispatch}) => next => action => {
    next(action)
  
    if (action.type === SHOW_WARNING) {
        alert(action.payload)
    }
}

export const uiMiddleware = [showErrorProcess, showWarningProcess]