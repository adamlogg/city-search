import {
    FETCH_CITIES,
    ERROR_FETCHING_CITIES,
    SUCCESS_FETCHING_CITIES,
} from '../actions/citiesActions'
import {
    updateCities,
    fetchingCitiesStarted,
    fetchingCitiesFinished,
} from '../actions/citiesActions'
import {getRequest} from '../actions/apiActions'
import {CITIES_URL} from '../urls'
import {showError} from '../actions/uiActions'


export const getCitiesProcess = ({dispatch}) => next => action => {
    next(action)
  
    if (action.type === FETCH_CITIES) {
        dispatch(fetchingCitiesStarted())
        const requestUrl = `${CITIES_URL}?city=${action.payload}`
        dispatch(getRequest(requestUrl, SUCCESS_FETCHING_CITIES, ERROR_FETCHING_CITIES))
    }
}

export const updateCitiesProcess = ({dispatch}) => next => action => {
    next(action)
  
    if (action.type === SUCCESS_FETCHING_CITIES) {
      dispatch(updateCities(action.payload.data))
      dispatch(fetchingCitiesFinished())
    }
}

export const fetchingFailedProcess = ({dispatch}) => next => action => {
    next(action)
  
    if (action.type === ERROR_FETCHING_CITIES) {
      dispatch(fetchingCitiesFinished())
      dispatch(showError(action.payload))
    }
}

export const citiesMiddleware = [getCitiesProcess, updateCitiesProcess, fetchingFailedProcess]
