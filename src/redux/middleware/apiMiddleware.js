import axios from 'axios'

import {GET_REQUEST} from "../actions/apiActions"


export const getRequestProcess = ({dispatch}) => next => action => {
    next(action)
    
    if(action.type === GET_REQUEST) {
        const {url, params, onSuccess, onError} = action.payload
        axios.get(url, params)
        .then((response) => dispatch({ type: onSuccess, payload: response.data }))
        .catch(error => dispatch({ type: onError, payload: error }))
    }
}

export const apiMiddleware = [getRequestProcess]