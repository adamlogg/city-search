import {applyMiddleware, createStore} from "redux"
import logger from 'redux-logger'

import {reducers} from './reducers'
import {citiesMiddleware} from './middleware/citiesMiddleware'
import {uiMiddleware} from './middleware/uiMiddleware'
import {apiMiddleware} from './middleware/apiMiddleware'


export const store = createStore(
    reducers,
    applyMiddleware(
        ...citiesMiddleware,
        ...uiMiddleware,
        ...apiMiddleware,
        logger,
    )
)