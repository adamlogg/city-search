import {
    UPDATE_CITIES,
    FETCHING_CITIES_STARTED,
    FETCHING_CITIES_FINISHED,
} from '../actions/citiesActions'


export function citiesReducer(state={fetchingInProgress: false, cities:[]}, action) {
    switch(action.type) {
        case UPDATE_CITIES:
            return {
                ...state,
                cities: action.payload,
            }
        case FETCHING_CITIES_STARTED:
            return {
                ...state,
                fetchingInProgress: true,
            }
        case FETCHING_CITIES_FINISHED:
            return {
                ...state,
                fetchingInProgress: false,
            }
        default:
            return state
    }
}