import {combineReducers} from 'redux'

import {citiesReducer} from './citiesReducer'


export const reducers = combineReducers({
    cities: citiesReducer,
})