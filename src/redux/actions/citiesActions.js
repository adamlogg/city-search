const prefix = '[cities]'

export const FETCH_CITIES = `${prefix} GET`
export const UPDATE_CITIES = `${prefix} UPDATE`
export const FETCHING_CITIES_STARTED = `${prefix} FETCHING_STARTED`
export const FETCHING_CITIES_FINISHED = `${prefix} FETCHING_FINISHED`
export const ERROR_FETCHING_CITIES = `${prefix} FETCHING_ERROR`
export const SUCCESS_FETCHING_CITIES = `${prefix} FETCHING_SUCCESS`

export const fetchCities = (searchString='') => ({
    type: FETCH_CITIES,
    payload: searchString,
})

export const fetchingCitiesStarted = () => ({
    type: FETCHING_CITIES_STARTED,
})

export const fetchingCitiesFinished = () => ({
    type: FETCHING_CITIES_FINISHED,
})

export const updateCities = (data) => ({
    type: UPDATE_CITIES,
    payload: data,
})