const prefix = '[api]'

export const GET_REQUEST = `${prefix} GET_REQUEST`

export const getRequest = (url, onSuccess, onError) => ({
    type: GET_REQUEST,
    payload: {url, onSuccess, onError}
})

