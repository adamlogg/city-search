const prefix = '[ui]'

export const SHOW_ERROR = `${prefix} SHOW_ERROR`
export const SHOW_WARNING = `${prefix} SHOW_WARNING`

export const showError = (error) => ({
    type: SHOW_ERROR,
    payload: error,
})

export const showWarning = (warning) => ({
    type: SHOW_WARNING,
    payload: warning,
})

